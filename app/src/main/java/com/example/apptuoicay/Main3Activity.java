package com.example.apptuoicay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.apptuoicay.calback.CallBackString;
import com.example.apptuoicay.service.PostMethod;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.apptuoicay.Config.linkServer;
import static com.example.apptuoicay.Config.tuoi5s;

public class Main3Activity extends AppCompatActivity {

    Button btn5s, btn10s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        addControlls();
        addEvents();
    }

    private void addControlls() {
        btn5s = findViewById(R.id.btn5s);
        btn10s = findViewById(R.id.btn10s);
    }

    private void addEvents() {
        btn5s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("time", "DuyHung");
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }

                new PostMethod(new CallBackString() {
                    @Override
                    public void doit(String str) {

                    }
                }).execute(linkServer + tuoi5s, jsonObject.toString());
            }
        });
    }
}
