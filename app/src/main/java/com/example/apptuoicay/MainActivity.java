package com.example.apptuoicay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.apptuoicay.calback.CallBackString;
import com.example.apptuoicay.model.Container;
import com.example.apptuoicay.service.GetMethod;
import com.google.gson.Gson;

import static com.example.apptuoicay.Config.getFirst;
import static com.example.apptuoicay.Config.linkServer;

public class MainActivity extends AppCompatActivity {

    Button btnWaterRing;
    ImageButton btnReset;
    TextView txtTemperature, txtHumidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControlls();
        addEvents();
    }

    private void addControlls() {
        btnWaterRing = findViewById(R.id.btnWaterRing);
        btnReset = findViewById(R.id.btnReset);
        txtHumidity = findViewById(R.id.txtHumidity);
        txtTemperature = findViewById(R.id.txtTemperature);

        getFirst();
    }

    private void addEvents() {
        btnWaterRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFirst();
            }
        });
    }

    private void getFirst(){
        new GetMethod(new CallBackString() {
            @Override
            public void doit(String str) {
                Container container = new Gson().fromJson(str, Container.class);
                txtTemperature.setText(container.getCelsiusTemp() + "*C");
                txtHumidity.setText(container.getHumidityTemp() + "%");
            }
        }).execute(linkServer + getFirst);
    }
}
