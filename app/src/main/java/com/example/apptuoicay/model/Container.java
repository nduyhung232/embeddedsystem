package com.example.apptuoicay.model;

public class Container {
    private String celsiusTemp;
    private String humidityTemp;

    public Container() {
    }

    public String getCelsiusTemp() {
        return celsiusTemp;
    }

    public void setCelsiusTemp(String celsiusTemp) {
        this.celsiusTemp = celsiusTemp;
    }

    public String getHumidityTemp() {
        return humidityTemp;
    }

    public void setHumidityTemp(String humidityTemp) {
        this.humidityTemp = humidityTemp;
    }
}
